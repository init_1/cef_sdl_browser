/**************************************************************************
 * @file cef_sdl_browser.hpp
 * @author Spartaco Amadei (spamadei@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-06-21
 *
 * @copyright Copyright (c) 2019
 *
 ***************************************************************************/
#ifndef CEF_SDL_BROWSER_HPP
#define CEF_SDL_BROWSER_HPP

#include "include/cef_client.h"
#include "include/cef_life_span_handler.h"
#include "include/cef_render_handler.h"
#include "include/cef_render_process_handler.h"
#include "include/internal/cef_ptr.h"
#include "include/wrapper/cef_message_router.h"

class SdlCefBrowserClient :
  public CefClient,
  public CefLifeSpanHandler,
  public CefRequestHandler {

public:
    explicit SdlCefBrowserClient(CefRefPtr<CefRenderHandler> ptr,
                                 const CefMessageRouterConfig& config);

    ~SdlCefBrowserClient();

    bool closeAllowed() const;
    
    // CefClient method
    CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() override;
    CefRefPtr<CefRenderHandler> GetRenderHandler() override;
    CefRefPtr<CefContextMenuHandler> GetContextMenuHandler() override;

    bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser,
                                  CefRefPtr<CefFrame> frame,
                                  CefProcessId source_process,
                                  CefRefPtr<CefProcessMessage> message) override;

    // CefLifeSpanMethod method
    void OnAfterCreated(CefRefPtr<CefBrowser> browser) override;
    bool DoClose(CefRefPtr<CefBrowser> browser) override;
    void OnBeforeClose(CefRefPtr<CefBrowser> browser) override;

    // CefRequestHandler method
    bool OnBeforeBrowse(CefRefPtr<CefBrowser> browser,
                        CefRefPtr<CefFrame> frame,
                        CefRefPtr<CefRequest> request,
                        bool user_gesture,
                        bool is_redirect) override;
    
    CefRefPtr<CefResourceHandler> GetResourceHandler(CefRefPtr<CefBrowser> browser,
                                                     CefRefPtr<CefFrame> frame,
                                                     CefRefPtr<CefRequest> request);
    
    void OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser,
                                   TerminationStatus status) override;
    
private:
    int browser_id;
    bool closing = false;

    CefRefPtr<CefRenderHandler> renderHandler;
    CefRefPtr<CefMessageRouterBrowserSide> messageRouterBrowserSide;
    CefRefPtr<CefContextMenuHandler> contextMenuHandler;
    CefMessageRouterBrowserSide::Handler* sampleMessageHandler;
    const CefMessageRouterConfig& config;

    IMPLEMENT_REFCOUNTING(SdlCefBrowserClient);
};

#endif  // CEF_SDL_BROWSER_HPP
