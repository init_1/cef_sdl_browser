macro(CREATE_EOS_ROOT ver src)
set(ARES_BIN_PATH "${CMAKE_BINARY_DIR}/EOS.${CMAKE_BUILD_TYPE}")
    if(NOT EXISTS ${ARES_BIN_PATH})
        make_directory("${ARES_BIN_PATH}")
    endif() 
    execute_process(
        COMMAND tar -xf "${src}/eos-dist-${ver}.tar.gz" -C "${ARES_BIN_PATH}"
    )
endmacro()