#!/bin/python
import tarfile, sys, os    

def make_tarfile(source_dir, version):
    output_filename = str("AresLauncher-Release-%", version)+".tar.gz"
    with tarfile.open(output_filename, "w:gz") as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))

def main():

    if len(sys.argv) == 0:
        print("no parameter given to script!")
        exit(1)

    srcDir = str(sys.argv[1])
    version = str(sys.atgv[2])

    make_tarfile(srcDir, version)
    


main()