/**
 * @file main.cpp
 * @author Spartaco Amadei (spamadei@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2019-06-23
 * 
 * @copyright Copyright (c) Spartaco Amadei, 2019
 * 
 */
#include "cef_sdl_app.hpp"
#include "cef_sdl_browser_client.hpp"
#include "cef_sdl_events.hpp"
#include "cef_sdl_handler.hpp"
#include "cef_sdl_configuration.hpp"

#include "include/cef_browser.h"
#include "include/cef_app.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <iostream>


/**
 * @brief  Cleanup SDL objects and shutdown CEF
 * 
 * @param window  pointer to SDL_Window
 * @param renderer  pointer to SDL_Renderer
 * @param cefIsInit  bool for specify if shutdown or not CEF process
 */
void cleanup(SDL_Window *window = nullptr, SDL_Renderer *renderer = nullptr, bool cefIsInit = false)
{
    if(cefIsInit){
        CefShutdown();
    }
    if(renderer){
        SDL_DestroyRenderer(renderer);
    }
    if(window){
        SDL_DestroyWindow(window);
    }
    SDL_Quit();
}

int main(int argc, char **argv){
    // Obtain conf from json
    Conf configJson;
    configJson.popoulateFromJson();

    CefMessageRouterConfig msgRouteCfg;
    CefRefPtr<SdlCefApp> cefApp = new SdlCefApp(msgRouteCfg);

    CefMainArgs args(argc, argv);
    auto exitCode = CefExecuteProcess(args, cefApp, nullptr);
    if(exitCode >= 0) {
        return exitCode;
    }

    // Initialize SDL device
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0){
        std::cerr << "[ERROR] [SDL] Can't initialize SDL! Error:" << SDL_GetError() << std::endl;
        return -1;
    }

    // Create SDL Window
    auto window = SDL_CreateWindow("CEF-SDL OFFSCREEN", 
                                    SDL_WINDOWPOS_UNDEFINED,
                                    SDL_WINDOWPOS_UNDEFINED,
                                    configJson.getWidth(),
                                    configJson.getHeight(),
                                    SDL_WINDOW_RESIZABLE); 
    if(!window){
        std::cerr << "[ERROR] [SDL] Can't create the window! Error:" << SDL_GetError() << std::endl;
        cleanup();
        return -1;
    }

    auto renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if(!renderer){
        std::cerr << "[ERROR] [SDL] Can't create renderer! Error:" << SDL_GetError() << std::endl;
        cleanup(window);
        return - 1;
    }

    //Chromium Embedded Framework initialization
    CefSettings settings;
    settings.windowless_rendering_enabled = true;
    settings.multi_threaded_message_loop = false;
    settings.external_message_pump = true;
    if(!CefInitialize(args, settings, cefApp, nullptr)) {
        std::cerr << "[ERROR] [CEF] Can't initialize Chrmoium Embedded Framework!";
        cleanup(window, renderer);
        return -1;
    }
    
    //Create the renderer handler
    CefRefPtr<SdlCefRenderHandler> renderHandler = new SdlCefRenderHandler(renderer,                   
                                                                           configJson.getWidth() ,
                                                                           configJson.getHeight());
    
    //Create the browser client that ties into the life cicle of the browser
    CefRefPtr<SdlCefBrowserClient> browserClient = new SdlCefBrowserClient(renderHandler, 
                                                                            msgRouteCfg); 

    // Some specified browser settings
    CefWindowInfo window_info;
    window_info.SetAsWindowless(kNullWindowHandle);

    CefBrowserSettings browserSettings;
    browserSettings.windowless_frame_rate = 60;     //Browser renderer frame rate (default is 30)
    browserSettings.background_color = 0;               //Trasparent backgrouns

    //Create the browser from current configuration
    CefRefPtr<CefBrowser> browser = CefBrowserHost::CreateBrowserSync(window_info, 
                                                                      browserClient,
                                                                      configJson.getLink(),
                                                                      browserSettings,
                                                                      nullptr,
                                                                      nullptr);

    
    while(!browserClient->closeAllowed()) {

        //Send events to the browser
        SDL_Event e;
        while(SDL_PollEvent(&e) != 0){
            //Quit and window resize
            if(e.type == SDL_QUIT) {
                browser->GetHost()->CloseBrowser(false);
            }
            else if (e.type == SDL_WINDOWEVENT && e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
                renderHandler->resize(e.window.data1, e.window.data2);
                browser->GetHost()->WasResized();
            }
            else {
                handleEvents(e, browser.get());
            }
        }

        // Cef main work loop
        cefApp->doCefWork();

        //Set background color
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        
        //Clear window/renderer
        SDL_RenderClear(renderer);
        renderHandler->render();

        //Update screen contents
        SDL_RenderPresent(renderer);
    }

    //clening up...
    browser = nullptr;
    browserClient = nullptr;
    renderHandler = nullptr;

    cleanup(window, renderer, true);
    return 0;
}