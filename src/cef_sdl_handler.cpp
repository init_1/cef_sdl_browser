/**
 * @file cef_sdl_handler.cpp
 * @author Spartaco Amadei (spamadei@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2019-06-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include <iostream>
#include "cef_sdl_handler.hpp"


SdlCefRenderHandler::SdlCefRenderHandler(SDL_Renderer *r, int w, int h):
    renderer{r},
    weidth{w},
    hight{h} 
{
    resize(w, h);
}


SdlCefRenderHandler::~SdlCefRenderHandler() {
    if(texture) {
        SDL_DestroyTexture(texture);
    }
    renderer = nullptr;
}

void SdlCefRenderHandler::GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) {
    rect = CefRect(0, 0, weidth, hight);
}

void SdlCefRenderHandler::OnPaint(CefRefPtr<CefBrowser> browser,
                                    PaintElementType type,
                                    const RectList &dirtyRects,
                                    const void *buffer, int w, int h) {
    if(w != weidth || h != hight){
        resize(w, h);
    }

    unsigned char* texture_data = nullptr;
    int texture_pitch = 0;
    size_t bufferSize = static_cast<size_t>(w) * static_cast<size_t>(h) * 4;

    paintingMutex.lock();
    SDL_LockTexture(texture, NULL, (void**)&texture_data, &texture_pitch);
    memcpy(texture_data, buffer, bufferSize);
    SDL_UnlockTexture(texture);
    paintingMutex.unlock();
}

void SdlCefRenderHandler::render() { 
    drawingMutex.lock(); 
    if(texture){
        SDL_RenderCopy(renderer, texture, NULL, NULL);
    }
    drawingMutex.unlock();
}

void SdlCefRenderHandler::resize(int w, int h){
    paintingMutex.lock();
    drawingMutex.lock();

    if (texture) {
        SDL_DestroyTexture(texture);
    }

    // Reassign the dimension of the CEF renderer
    weidth = w;
    hight  = h;

    //recreate the texture
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888,
                                 SDL_TEXTUREACCESS_STREAMING, w, h);
    SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);

    paintingMutex.unlock();
    drawingMutex.unlock();
}