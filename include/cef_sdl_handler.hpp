/**
 * @file cef_sdl_handler.hpp
 * @author Spartaco Amadei (spamadei@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2019-06-20
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#ifndef CEF_SDL_HANDLER_HPP
#define CEF_SDL_HANDLER_HPP

#include <SDL2/SDL.h>
#include "include/cef_render_handler.h"

#include <mutex>
/**
 * @brief  Class to handle the SDL randerer and print to it
 * the cef offscreen
 */
class SdlCefRenderHandler : public CefRenderHandler
{
    public:
        /**
         * @brief Construct a new Sdl Cef Render Handler object 
         * 
         * @param r  The pointer to SDL Renderer to handler
         * @param w  The width of the renderer
         * @param h  The heioght of the renderer
         */
        SdlCefRenderHandler(SDL_Renderer *r, int w, int h);
        ~SdlCefRenderHandler() override;
        /**
         * @brief  Resize the CEF renderer
         * 
         * @param w Weigth 
         * @param h Height
         */
        void resize(int w, int h);

        /**
         * @brief  Copy the render to SDL Renderer
         * 
         */
        void render();

        void GetViewRect(CefRefPtr<CefBrowser> browser,
                                        CefRect &rect) override;
        /**
         * @brief  Copy texture from CEF  to SDL
         * 
         * @param browser  Pointer to CefBrowser
         * @param type  The type of element to draw
         * @param dirtyRects  A refs to a RectList
         * @param buffer  the buffer that contain texture to copy
         * @param w The texture to copy width
         * @param h  The texture to copy height
         */
        void OnPaint(CefRefPtr<CefBrowser> browser,
                                PaintElementType type,
                                const RectList &dirtyRects,
                                const void *buffer, int w, int h) override;

    private:
        int weidth;
        int hight;

        SDL_Texture *texture = nullptr;
        SDL_Renderer *renderer = nullptr;

        std::mutex paintingMutex;
        std::mutex drawingMutex;

        IMPLEMENT_REFCOUNTING(SdlCefRenderHandler);
};

#endif // !CEF_SDL_HANDLER_HPP
