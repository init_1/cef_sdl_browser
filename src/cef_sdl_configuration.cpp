#include "cef_sdl_configuration.hpp"

Conf::Conf(const std::string& json, int width, int height):
    winWidth{width},
    winHeight{height}, 
    configPath{json}{
    configJson.open(configPath);
    if(!configJson){
        createJsonScheleton();
    }
}
void Conf::createJsonScheleton(){
    configJson.open(configPath, std::fstream::in | std::fstream::out | std::fstream::trunc);
    configJson << "{\"webLink\": \"https://www.google.com\"}";
    configJson.close();
}
void Conf::popoulateFromJson(){
    json j;
    configJson >> j;                            // Read conf from the json
    if(j["webLink"].is_string())
        webLink = j["webLink"].get<std::string>();
    else{
        std::cerr << "[ERROR] [CONFIGURATION] No valid webLink value!\n";
        exit(-1);
    }

    if(j["winWidth"].is_number_integer()){
        winWidth = j["winWidth"].get<int>();
    }

    if(j["winHeight"].is_number_integer()){
        winHeight = j["winHeight"].get<int>();
    }
}