/**
 * @file cef_sdl_configuration.hpp
 * @author Spartaco Amadei (spamadei@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2019-07-03
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include <nlohmann/json.hpp>
#include <SDL2/SDL.h>

#include <iostream>
#include <fstream>

const std::string BASE_PATH = SDL_GetBasePath(); //get the base path of the executable
const std::string jsonPath = BASE_PATH + "config.json";

using json = nlohmann::json;

class Conf
{
public:
    Conf(const std::string& json = jsonPath, int width = 800, int height = 800);
    ~Conf() { configJson.close(); }

    void popoulateFromJson();

    int getWidth() { return winWidth; }
    int getHeight() { return winHeight; }
    std::string getLink() { return webLink; }

private:
    void createJsonScheleton();
    std::string webLink;
    int winWidth;
    int winHeight;
    const std::string configPath;

    std::fstream configJson;
};
#endif