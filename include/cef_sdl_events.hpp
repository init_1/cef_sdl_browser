/**
 * @file cef_sdl_events.hpp
 * @author Spartaco Amadei (spamadei@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2019-06-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef CEF_SDL_EVENTS_HPP
#define CEF_SDL_EVENTS_HPP

#include <SDL2/SDL.h>
#include <include/cef_browser.h>

#include <iostream>

#define _SDLK_KP0          1073741912
#define _SDLK_KP1          1073741913
#define _SDLK_KP2          1073741914
#define _SDLK_KP3          1073741915
#define _SDLK_KP4          1073741916
#define _SDLK_KP5          1073741917
#define _SDLK_KP6          1073741918
#define _SDLK_KP7          1073741919
#define _SDLK_KP8          1073741920
#define _SDLK_KP9          1073741921
#define _SDLK_SCROLL_LOCK  1073741895
#define _SDLK_INSERT       1073741897
#define _SDLK_HOME         1073741898
#define _SDLK_PAGEUP       1073741899
#define _SDLK_END          1073741901
#define _SDLK_PAGEDOWN     1073741902
#define _SDLK_NUM          1073741907
#define _SDLK_NUM_DIVIDE   1073741908
#define _SDLK_NUM_MULTIPLY 1073741909
#define _SDLK_NUM_SUBTRACT 1073741910
#define _SDLK_NUM_ADD      1073741911
#define _SDLK_NUM_DECIMAL  1073741923
#define _SDLK_SELECT       1073741925

const int DEFAULT_KEY_CODE = 0;
const int DEFAULT_CHAR_CODE = -1;

/**
 * @namespace SmartEvent
 */
namespace SmartEvent {

/**
 * @class @c CefSdlMouseEvent 
 * @brief Object that rapresent a Cef from SDL mouse event
 *        It support the assignment opration with @c SDL_MouseButtonEvent,
 *        @c SDL_TouchFingerEvent, and @c SDL_MouseWheelEvent
 *  
 * @param browser The smartPointer to your CefBrowser
 */
class CefSdlMouseEvent {
public:
    CefSdlMouseEvent(CefBrowser* browser);
    ~CefSdlMouseEvent();

    /**
     * @brief Send click event to cefBrowser
     * 
     * @param t type 
     * @param mouseUp  
     * @param clickNum  
     */
    void injectClickEvent(bool mouseUp = true, 
                          cef_mouse_button_type_t t = MBT_LEFT, 
                          int clickNum=1);
    
    /**
     * @brief Send mouse mooviment event to cef browser
     * 
     * @param mouseLeave 
     */
    void injectMoveEvent(bool mouseLeave=false);
    
    /**
     * @brief Send mouse wheel eventto CEF browser
     */
    void injectWheelEvent();

    // assignment operator
    CefSdlMouseEvent& operator=(const SDL_MouseButtonEvent&);
    CefSdlMouseEvent& operator=(const SDL_TouchFingerEvent&);
    CefSdlMouseEvent& operator=(const SDL_MouseWheelEvent&);
    CefSdlMouseEvent& operator=(const SDL_MouseMotionEvent&);

private:
    CefBrowser* browser = nullptr;
    CefMouseEvent event;
    
    int offsetX;
    int offsetY;
    
    Uint32 offsetDirection;
    
    double pressure;
};
}


/**
 * @brief  Handle the SDL Events and send the properly events to CEF Browser
 * 
 * @param e  The SDL Event
 * @param browser  a pointer to cef browser
 */
void handleEvents(SDL_Event& e, CefBrowser* browser);

#endif // CEF_SDL_EVENTS_HPP
