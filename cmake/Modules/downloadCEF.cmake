function(downloadCEF version download_dir)
    if(NOT EXISTS ${download_dir})
        execute_process(COMMAND mkdir "${download_dir}")
    endif()
    set(CEF_DIST "cef_binary_${version}_linux64")
    set(CEF_DOWNLOAD_DIR "${download_dir}")
    set(CEF_ROOT "${CEF_DOWNLOAD_DIR}/cef" CACHE INTERNAL "CEF_ROOT")
    set(CEF_DOWNLOAD_FILENAME "${CEF_DIST}.tar.bz2")
    set(CEF_DOWNLOAD_PATH "${CEF_DOWNLOAD_DIR}/${CEF_DOWNLOAD_FILENAME}")
    set(CEF_DOWNLOAD_URL "http://opensource.spotify.com/cefbuilds/${CEF_DOWNLOAD_FILENAME}")

    #download CEF bin...
    if(NOT EXISTS "${CEF_DOWNLOAD_PATH}")
        message(STATUS "Downloading ${CEF_DOWNLOAD_FILENAME}...")
        if(EXISTS ${CEF_ROOT})
        file(REMOVE_RECURSE ${CEF_ROOT})
        endif() 
        file(
            DOWNLOAD "${CEF_DOWNLOAD_URL}" "${CEF_DOWNLOAD_PATH}"
            SHOW_PROGRESS
            )
        if(NOT EXISTS "${CEF_DOWNLOAD_PATH}")
            message(FATAL_ERROR "Can't download CEF")
        endif()
    endif()


    #execute_process(
    #     COMMAND wget  "${CEF_DOWNLOAD_URL}"  -O "${CEF_DOWNLOAD_PATH}"
    #)

            #extract 
    if(NOT EXISTS "${CEF_ROOT}")
        make_directory("${CEF_ROOT}")
    endif()
    
    if(NOT EXISTS "${CEF_ROOT}/CMakeLists.txt")
        message(STATUS "Extracting Chromium Embedded Framework...")
        execute_process(
            COMMAND rm -rf "${CEF_ROOT}/*"
            COMMAND tar -xf "${CEF_DOWNLOAD_PATH}" -C "${CEF_ROOT}" --strip-components=1
        )
    endif()

    #message(STATUS "PATCHING CEF...")
    #execute_process(
     #   COMMAND  sh "${CMAKE_MODULE_PATH}/patchCmake.sh" "${CEF_ROOT}" 
    #)

endfunction()
