#!/bin/bash
if [[ ! -e "${1}/PATCHED" ]]; then
    echo "OK!"
    sed 's/-Werror/#-Werror/g' -i ${1}/cmake/cef_variables.cmake
    sed '/^add_subdirectory[\(]tests[\/].*/d' -i ${1}/CMakeLists.txt
    touch ${1}/PATCHED
else
    echo "The patch is alredy present!"
fi