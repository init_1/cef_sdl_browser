# CHROMIUM SDL WEB BROWSER ([CEF](https://bitbucket.org/chromiumembedded/cef) BASED)
This project bornn to make an efficent and working example of cef offscreen rendering with SDL2 application. I have maked this application for work purpose, I needed to implement a Chromium browser to a previus created SDL application, for permit the execution of some online game on it.
In the next, since exists very less good and updated example of  in general CEF used in offscreen and, among other things, neither one serius example of CEF offscreen used with SDL, I have decised to pubblish my work as example for someone should need of work with CEF and SDL togeter.
Feel you free to do what you want and use as you prefer my code.
Only, if you have appreciated my effort and this code was usless for you, please, take me a star and if you think to reuse my code, modding it or integrate it into your project, quote me. Thanks :)

#### TRY IT
> ##### COMPILE ISTRUCTION
> For compile this project run the following command:
>
>```
>mkdir build 
>cd build
>cmake .. && cmake --build --target OffCefBrowser
>```

>##### RUN AND USING 
> You will find the builded binary into `build/${CMAKE_BUILD_TYPE}` folder.
> For example if your `CMAKE_BUILD_TYPE` is *"Debug"* 
> ```
> cd build/Debug 
> ```
> You can run the program using 
> ```
> ./OffCefBrowser
> ```
> The first time that you running it may be crash because the *confi.json* config file doesn't exist yet, futhermore it will create it if it not alredy exist. You can change the web address of the brawser by editing the `webLink` of the json config file.

#### SOME TIPS
 if you want change the link  you need to change the ```"webLink"``` value in config.json with what you want.
 if you want change the default height/wigth you can use the ```"winHeight"``` and  ```"winWidth"``` variables.