/***************************************************************************
 * @file cef_sdl_browser_client.cpp                                        *
 * @author Spartaco Amadei (spamadei@gmail.com)                            *
 * @brief                                                                  *
 * @version 0.1                                                            *
 * @date 2019-06-21                                                        *
 *                                                                         *
 * @copyright Copyright (c) 2019                                           *
 *                                                                         *
 ***************************************************************************/
#include "include/wrapper/cef_helpers.h"
#include "include/cef_parser.h"

#include "cef_sdl_browser_client.hpp"

class SampleMessageHandler : 
  public CefMessageRouterBrowserSide::Handler {
public:
    bool OnQuery(CefRefPtr<CefBrowser> browser,
                 CefRefPtr<CefFrame> frame,
                 int64 query_id,
                 const CefString& request,
                 bool persistent,
                 CefRefPtr<Callback> callback) override {
        
        //parse here "reqs" for get any information
        CefRefPtr<CefDictionaryValue> result_dict = 
          CefDictionaryValue::Create();
        result_dict->SetInt("count", ++counter);

        CefRefPtr<CefValue> value = CefValue::Create();
        value->SetDictionary(result_dict);
        CefString json = CefWriteJSON(value, JSON_WRITER_DEFAULT);

        callback.get()->Success(json);
        return true;
    }

    void OnQueryCanceled(CefRefPtr<CefBrowser>,
                         CefRefPtr<CefFrame> frame,
                         int64 query_id) override {}

private:
    int counter = 0;
};

/**
 * Prevent to context menu to be shown
 */
class NoContextMenu : 
  public CefContextMenuHandler {
    void OnBeforeContextMenu(CefRefPtr<CefBrowser> browser,
                             CefRefPtr<CefFrame> frame,
                             CefRefPtr<CefContextMenuParams> params,
                             CefRefPtr<CefMenuModel> model) override {
        model->Clear();
    }
    
    IMPLEMENT_REFCOUNTING(NoContextMenu);
};

// BrowserClint Class Implementation

SdlCefBrowserClient::SdlCefBrowserClient(CefRefPtr<CefRenderHandler> ptr, 
                                         const CefMessageRouterConfig &config):
    renderHandler{ptr},
    contextMenuHandler(new NoContextMenu),
    config(config) {}

SdlCefBrowserClient::~SdlCefBrowserClient() = default;

CefRefPtr<CefLifeSpanHandler> SdlCefBrowserClient::GetLifeSpanHandler() {
    return this;
}

CefRefPtr<CefRenderHandler> SdlCefBrowserClient::GetRenderHandler(){
    return renderHandler;
}

CefRefPtr<CefContextMenuHandler> SdlCefBrowserClient::GetContextMenuHandler(){
    return contextMenuHandler;
}

void SdlCefBrowserClient::OnAfterCreated(CefRefPtr<CefBrowser> browser) {
    CEF_REQUIRE_UI_THREAD();

    browser_id = browser->GetIdentifier();

    if(!messageRouterBrowserSide){
        messageRouterBrowserSide = CefMessageRouterBrowserSide::Create(config);

        sampleMessageHandler = new SampleMessageHandler;
        messageRouterBrowserSide->AddHandler(sampleMessageHandler, false);
    }
}

bool SdlCefBrowserClient::DoClose(CefRefPtr<CefBrowser> browser){
    CEF_REQUIRE_UI_THREAD();           //Must t6o be executed on the UI thread

    if(browser->GetIdentifier() == browser_id){
        closing = true;
    }
    return false;
}

void SdlCefBrowserClient::OnBeforeClose(CefRefPtr<CefBrowser> browser) {
    messageRouterBrowserSide->OnBeforeClose(browser);

    CEF_REQUIRE_UI_THREAD();

    messageRouterBrowserSide->RemoveHandler(sampleMessageHandler);
    delete sampleMessageHandler;
    messageRouterBrowserSide = nullptr;
}

bool SdlCefBrowserClient::closeAllowed() const { return closing; }

bool SdlCefBrowserClient::OnBeforeBrowse(CefRefPtr<CefBrowser> browser,
                                         CefRefPtr<CefFrame> frame,
                                         CefRefPtr<CefRequest> request,
                                         bool use_gesture,
                                         bool is_redirect){
    CEF_REQUIRE_UI_THREAD();

    messageRouterBrowserSide->OnBeforeBrowse(browser, frame);
    return false;
}

CefRefPtr<CefResourceHandler> SdlCefBrowserClient::GetResourceHandler(CefRefPtr<CefBrowser> browser,
                                                                      CefRefPtr<CefFrame> frame,
                                                                      CefRefPtr<CefRequest> request){
    CEF_REQUIRE_UI_THREAD();

    return nullptr;
}

void SdlCefBrowserClient::OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser,
                                                    TerminationStatus status ) {
    CEF_REQUIRE_UI_THREAD();

    messageRouterBrowserSide->OnRenderProcessTerminated(browser);
}

bool SdlCefBrowserClient::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser,
                                                   CefRefPtr<CefFrame> source_frame,
                                                   CefProcessId source_process,
                                                   CefRefPtr<CefProcessMessage> message){
    CEF_REQUIRE_UI_THREAD();
    return messageRouterBrowserSide->OnProcessMessageReceived(browser, source_frame,
                                                                                                                source_process, message);
}

